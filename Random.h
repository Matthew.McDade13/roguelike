//
// Created by matt on 9/13/18.
//

#ifndef ROGUELIKE_RANDOM_H
#define ROGUELIKE_RANDOM_H

template <typename T>
struct Range
{
    T min;
    T max;
};

float randomf(float max);
float randomf(float min, float max);
float randomf(Range<float> range);

int random(int max);
int random(int min, int max);
int random(Range<int> range);


using Rangei = Range<int>;
using Rangef = Range<float>;

#endif //ROGUELIKE_RANDOM_H
