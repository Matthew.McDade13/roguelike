//
// Created by matt on 9/16/18.
//

#include "EntityManager.h"
#include <cassert>

uint32_t EntityManager::add(std::unique_ptr<Entity> entity)
{
    if (m_firstAvail >= m_entities.size())
    {
        const auto entityIndx = static_cast<uint32_t>(m_entities.size());
        m_firstAvail = (entityIndx + 1);
        m_entityLookup.push_back({ entityIndx, m_firstAvail });
        m_entities.push_back(std::move(entity));
        return entityIndx;
    }

    EntityElem elem = m_entityLookup[m_firstAvail];
    m_entities[elem.index] = std::move(entity);
    m_firstAvail = elem.next;
    return elem.index;
}

void EntityManager::destroy(uint32_t id)
{
    assert(id >= 0 && id < m_entities.size());
    EntityElem& destroyTarget = m_entityLookup[id];

    m_entities[destroyTarget.index].reset();

    destroyTarget.next = m_firstAvail;
    m_firstAvail = destroyTarget.index;
}

Entity *EntityManager::get(uint32_t id)
{
    return m_entities[id].get();
}

const std::vector<Entity_Ptr> &EntityManager::entities() const
{
    return m_entities;
}
