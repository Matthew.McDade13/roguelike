//
// Created by matt on 9/10/18.
//

#ifndef ROGUELIKE_LEVELMAP_H
#define ROGUELIKE_LEVELMAP_H

#include <Pure2D/Math/Vec2.h>
#include "Spritemap.h"
#include "Random.h"

namespace pure { struct Texture; }

struct LevelMap
{
    Spritemap map;

    std::vector<pure::Recti> rooms;

    Rangei numRooms;
    Rangei roomSize;
    Rangei hallwaySize;

    LevelMap(const pure::Texture& mapTexture, pure::Vec2i mapSize, pure::Vec2i tileSize);

    void generate();
};


#endif //ROGUELIKE_LEVELMAP_H
