//
// Created by matt on 9/28/18.
//

#ifndef ROGUELIKE_ASSETMANAGER_H
#define ROGUELIKE_ASSETMANAGER_H

#include <cinttypes>
#include <type_traits>
#include <unordered_map>
#include "Util/Ext.h"
#include <Pure2D/System/NonCopyable.h>
#include <Pure2D/Graphics/Shader.h>
#include <Pure2D/Graphics/Texture.h>
#include <Pure2D/Graphics/Mesh.h>

// TODO: Maybe move this back to Pure2D lib and just add a freeResources method instead of a dtor?
struct AssetManager : private pure::NonCopyable
{
    static AssetManager& get();
    void free();

    template<typename T>
    void freeResource(const char *name)
    {
        if constexpr(std::is_same_v<T, pure::Mesh>)
            getMap<T>()[name].vbo.free();
        else
            getMap<T>()[name].free();
    }

    bool loadShader(const char* shaderName, const char* vertPath, const char* fragPath);
    bool loadShader(const char* shaderName, pure::Shader shader);
    bool loadShaderSrc(const char* shaderName, const char* vertSrc, const char* fragSrc);

    bool loadTexture(const char* filePath, bool shouldFlip = true);
    bool loadMesh(const char* name, pure::Mesh m_mesh);

    template<typename T>
    T* get(const char* name)
    {
        return mapGet(getMap<T>(), std::string(name));
    }

private:
    std::unordered_map<std::string, pure::Shader> m_shaders;
    std::unordered_map<std::string, pure::Mesh> m_meshes;
    std::unordered_map<std::string, pure::Texture> m_textures;

    template<typename T>
    std::unordered_map<std::string, T>& getMap()
    {
        if constexpr(std::is_same_v<T, pure::Shader>)
        {
            return m_shaders;
        }
        else if constexpr(std::is_same_v<T, pure::Texture>)
        {
            return m_textures;
        }
        else if constexpr(std::is_same_v<T, pure::Mesh>)
        {
            return m_meshes;
        }
    }

    AssetManager() = default;
    ~AssetManager() = default;
};


#endif //ROGUELIKE_ASSETMANAGER_H
