//
// Created by matt on 9/9/18.
//

#ifndef ROGUELIKE_SPRITEMAP_H
#define ROGUELIKE_SPRITEMAP_H

#include <vector>
#include <string>
#include <Pure2D/Graphics/Transform.h>
#include <Pure2D/Math/Vec2.h>
#include <Pure2D/Math/Rect.h>
#include <Pure2D/Graphics/Renderable.h>
#include <Pure2D/Graphics/SpriteBatch.h>

namespace pure
{
    struct Texture;
    struct Shader;
}

struct Spritemap : public pure::Renderable
{
    struct Tile
    {
        // Enum val maps to index in m_tileTexRects array
        enum Type
        {
            WALL = 0,
            FLOOR,
            NUM_TYPES
        } type;

        pure::Transform transform;
    };
    pure::Vec2i size;
    pure::Vec2i tileSize;

    Spritemap(const pure::Texture& mapTexture, pure::Vec2i size, pure::Vec2i tileSize);

    void resetTiles();

    void setTileType(int x, int y, Tile::Type tile);

    pure::Vec2i tileCoordAt(pure::Vec2f worldPos);
    Tile& at(int x, int y);
	Tile& at(pure::Vec2i pos);

    Tile::Type tileTypeAt(pure::Vec2i pos) const;
	Tile::Type tileTypeAt(int x, int y) const;

    void addTileRect(const pure::Rectui& texRect, Tile::Type tileType);

    void updateTiles();

    void draw(pure::Renderer& renderer) final;

    void setFragShader(const std::string& shaderSrc);
    const pure::Shader& shader() const;

private:
    const pure::Texture* m_texture;
    std::vector<pure::Rectui> m_tileTexRects;
    std::vector<Tile> m_tiles;
    pure::SpriteBatch m_sprites;
};




#endif //ROGUELIKE_SPRITEMAP_H
