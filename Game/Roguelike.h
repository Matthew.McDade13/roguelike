//
// Created by matt on 9/16/18.
//

#ifndef ROGUELIKE_GAME_H
#define ROGUELIKE_GAME_H

#include <memory>
#include <vector>
#include <optional>
#include "Game/LevelMap.h"
#include <Pure2D/Engine/Game.h>
#include <Pure2D/Graphics/Mesh.h>
#include <Pure2D/Audio/AudioSource.h>
#include <Pure2D/Audio/Music.h>
#include <Pure2D/Graphics/RenderTexture.h>
#include <Pure2D/Graphics/Transform.h>
#include <Pure2D/Graphics/Font.h>
#include "Entities/Player.h"
#include "EntityManager.h"

namespace pure 
{ 
	struct WindowEvent; 
	template<typename T> struct Rect;
}

struct Roguelike final : public pure::Game
{
    Player player;
    std::optional<LevelMap> level;
    EntityManager entities;

	Roguelike();
	~Roguelike() final;

	pure::Window& window();
	pure::Camera& camera();

    Entity* getPlayerEntity();
	bool isInView(const pure::Rect<float>& r);

private:
	pure::Mesh m_cursorOutline;
	pure::Transform m_outlineTransf;
	pure::Font m_font;
	pure::Text m_text;
	pure::AudioSource m_sound;
	pure::Music m_music;
	pure::RenderTexture m_renderTexture;
	pure::Shader m_spotShader;

	void update(float dt) final;
	void load() final;
	void draw() final;
	void windowEvent(pure::WindowEvent& event) final;
};


#endif //ROGUELIKE_GAME_H
