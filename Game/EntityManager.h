//
// Created by matt on 9/16/18.
//

#ifndef ROGUELIKE_ENTITYMANAGER_H
#define ROGUELIKE_ENTITYMANAGER_H

#include <memory>
#include <vector>

#include "Entities/Entity.h"

using Entity_Ptr = std::unique_ptr<Entity>;

struct EntityManager
{
private:
    struct EntityElem
    {
        uint32_t index;
        uint32_t next;
    };

public:
    EntityManager(const EntityManager&) = delete;
    EntityManager& operator=(const EntityManager&) = delete;

    EntityManager() = default;

    inline size_t size() const { return m_entities.size(); }
    uint32_t add(Entity_Ptr entity);
    void destroy(uint32_t id);

    Entity* get(uint32_t id);

    const std::vector<Entity_Ptr>& entities() const;

private:
    std::vector<EntityElem> m_entityLookup;
    std::vector<Entity_Ptr> m_entities;
    uint32_t m_firstAvail = 0;
};


#endif //ROGUELIKE_ENTITYMANAGER_H
