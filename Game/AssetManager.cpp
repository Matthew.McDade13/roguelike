//
// Created by matt on 9/28/18.
//
#include "AssetManager.h"

using namespace pure;

bool AssetManager::loadShader(const char *shaderName, const char *vertPath, const char *fragPath)
{
    Shader shader = Shader::fromFile(vertPath, fragPath);
    if (shader.id() == 0) return false;

    return loadShader(shaderName, shader);
}

bool AssetManager::loadShader(const char *shaderName, pure::Shader shader)
{
    auto result = m_shaders.emplace(shaderName, shader);
    return result.second;

}

bool AssetManager::loadShaderSrc(const char *shaderName, const char *vertSrc, const char *fragSrc)
{
    Shader shader = Shader::fromSrc(vertSrc, fragSrc);
    if (shader.id() == 0) return false;

    return loadShader(shaderName, shader);
}

bool AssetManager::loadTexture(const char *filePath, bool shouldFlip)
{
    Texture tex = Texture::make(filePath, shouldFlip);
    if (tex.id_ == 0) return false;

    auto result = m_textures.emplace(filePath, tex);
    return result.second;
}

bool AssetManager::loadMesh(const char *name, pure::Mesh m_mesh)
{
    auto result = m_meshes.emplace(name, m_mesh);
    return result.second;
}

void AssetManager::free()
{
    for (auto& kv : m_shaders)
        kv.second.free();
    for (auto& kv : m_textures)
        kv.second.free();
    for (auto& kv : m_meshes)
        kv.second.vbo.free();
}

AssetManager &AssetManager::get()
{
    static AssetManager manager{};
    return manager;
}
