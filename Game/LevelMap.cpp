//
// Created by matt on 9/10/18.
//

#include "LevelMap.h"
#include "Util.h"
#include <Pure2D/Math/Rect.h>
#include <cassert>
#include <cmath>
#include <iostream>
#include <Pure2D/Graphics/Renderer.h>

using namespace pure;

static void bresenhamHallway(Spritemap& map, Vec2i from, Vec2i to, float width);

static inline bool isInRangePos(int val, int posRange) { return val < posRange && val >= 0; }

LevelMap::LevelMap(const pure::Texture& mapTexture, pure::Vec2i mapSize, pure::Vec2i tileSize):
    map(mapTexture, mapSize, tileSize)
{ }


// TODO: hallways that got right and then up look off center
void LevelMap::generate()
{
    const int numOfRooms = random(this->numRooms);

    rooms.clear();
    rooms.reserve(uint32_t(numOfRooms));

    for (int i = 0; i < numOfRooms; i++)
    {
        Recti room = {};
        room.x = random(map.size.x);
        room.y = random(map.size.y);
        room.w = random(roomSize);
        room.h = random(roomSize);

        if (room.right() > map.size.x)
            room.x -= room.right() - map.size.x;
        if (room.bottom() > map.size.y)
            room.y -= room.bottom() - map.size.y;


        for(int y = 0; y < room.h; y++)
        {
            const int yPos = room.y + y;
            for (int x = 0; x < room.w; x++)
            {
                const int xPos = room.x + x;

                if (xPos < map.size.x && yPos < map.size.y)
                    map.setTileType(xPos, yPos, Spritemap::Tile::FLOOR);
            }
        }

        rooms.push_back(room);
    }

    for (size_t i = 0; i < rooms.size() - 1; i++)
    {
        const Recti& curr = rooms[i];
        const Recti& next = rooms[i + 1];
        const Vec2i startpoint = { random(curr.x, curr.right() - 1), random(curr.y, curr.bottom() - 2) };
        const Vec2i endpoint = { random(next.x, next.right() - 1), random(next.y, next.bottom() - 1) };

        const int xOffset = signum(endpoint.x - startpoint.x);
        const int yOffset = signum(endpoint.y - startpoint.y);

        int x = startpoint.x;

        constexpr int hallwayWidth = 3;

        {
            for (; x != endpoint.x; x += xOffset)
            {
                for (int j = 0; j < hallwayWidth; j++)
                {
                    const int hallwayY = clamp(startpoint.y + j, 0, map.size.y - 1);
                    map.setTileType(x, hallwayY, Spritemap::Tile::FLOOR);
                }
            }
        }


        {
            for (int y = startpoint.y; y != endpoint.y; y += yOffset)
            {
                for (int j = 0; j < hallwayWidth; j++)
                {
                    const int hallwayX = clamp(x + j, 0, map.size.x - 1);
                    map.setTileType(hallwayX, y, Spritemap::Tile::FLOOR);
                }
            }
        }
    }

    map.updateTiles();

}

// NOTE: Not sure if we actually want to use this algo for drawing hallways, but ill keep it here for future reference
void bresenhamHallway(Spritemap& map, Vec2i from, Vec2i to, float width)
{
    int x0 = from.x, y0 = from.y;
    int x1 = to.x, y1 = to.y;


    int dx = abs(x1-x0), sx = x0 < x1 ? 1 : -1;
    int dy = abs(y1-y0), sy = y0 < y1 ? 1 : -1;
    int err = dx-dy, e2, x2, y2;                          /* error value e_xy */
    float ed = dx+dy == 0 ? 1 : sqrtf((float)dx*dx+(float)dy*dy);

    for (width = (width+1)/2; ; ) /* pixel loop */
    {
        map.setTileType(x0,y0, Spritemap::Tile::FLOOR);
        e2 = err; x2 = x0;
        if (2*e2 >= -dx)  /* x step */
        {
            for (e2 += dy, y2 = y0; e2 < ed*width && (y1 != y2 || dx > dy); e2 += dx)
                map.setTileType(x0, y2 += sy, Spritemap::Tile::FLOOR);
            if (x0 == x1) break;
            e2 = err; err -= dy; x0 += sx;
        }
        if (2*e2 <= dy)  /* y step */
        {
            for (e2 = dx-e2; e2 < ed*width && (x1 != x2 || dx < dy); e2 += dy)
                map.setTileType(x2 += sx, y0, Spritemap::Tile::FLOOR);
            if (y0 == y1) break;
            err += dx; y0 += sy;
        }
    }
}
