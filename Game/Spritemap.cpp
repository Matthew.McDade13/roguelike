//
// Created by matt on 9/9/18.
//

#include "Spritemap.h"
#include "Util.h"
#include <Pure2D/Graphics/Renderer.h>
#include <Pure2D/Graphics/Texture.h>
#include <Pure2D/Graphics/Quad.h>
#include <Pure2D/Graphics/Mesh.h>
#include <iostream>
#include <cmath>

using namespace pure;

Spritemap::Spritemap(const pure::Texture& mapTexture, pure::Vec2i size, pure::Vec2i tileSize):
    size(size), tileSize(tileSize), m_texture(&mapTexture), m_sprites(mapTexture, size_t(size.x * size.y)) //  m_batch(mapTexture, size_t(size.x * size.y) * 2)
{
    m_tileTexRects.resize(Tile::Type::NUM_TYPES);
    resetTiles();
}

void Spritemap::resetTiles()
{

    m_tiles.clear();

    {
        const size_t mapSize = size_t(size.x) * size_t(size.y);
        m_tiles.reserve(mapSize);
    }

    for (int i = 0; i < size.x; i++)
    {
        for (int j = 0; j < size.y; j++)
        {
            Tile t = {};
            t.transform.setPosition({ j * float(tileSize.x), i * float(tileSize.y), 0.f });
            t.transform.setSize({ float(tileSize.x), float(tileSize.y) });
            t.type = Tile::WALL;
            m_tiles.push_back(t);
        }
    }

    m_sprites.clear();
}

Spritemap::Tile& Spritemap::at(int x, int y)
{
    return indexFlat2DArr(&m_tiles[0], size.x, x, y);
}

Spritemap::Tile & Spritemap::at(pure::Vec2i pos)
{
	return at(pos.x, pos.y);
}

Spritemap::Tile::Type Spritemap::tileTypeAt(pure::Vec2i pos) const
{
	return tileTypeAt(pos.x, pos.y);
}

Spritemap::Tile::Type Spritemap::tileTypeAt(int x, int y) const
{
    return indexFlat2DArr(&m_tiles[0], size.x, x, y).type;
}

void Spritemap::setTileType(int x, int y, Tile::Type tile)
{
    Tile& t = at(x, y);
    t.type = tile;
}

void Spritemap::draw(pure::Renderer &renderer)
{
    renderer.draw(m_sprites);
}

void Spritemap::addTileRect(const pure::Rectui& texRect,Tile::Type tileType)
{
    m_tileTexRects[size_t(tileType)] = texRect;
}

void Spritemap::updateTiles()
{
    for (auto &t : m_tiles)
    {
        Quad q = Quad::make();
        q.setTextureCoords(m_tileTexRects[size_t(t.type)], m_texture->size);
        m_sprites.submit(q, t.transform.modelMatrix());
    }

	m_sprites.flush();
}

void Spritemap::setFragShader(const std::string &shaderSrc)
{
    m_sprites.setFragShader(shaderSrc.c_str());
}

const pure::Shader &Spritemap::shader() const
{
    return m_sprites.shader();
}

pure::Vec2i Spritemap::tileCoordAt(pure::Vec2f worldPos)
{
    const Vec2i worldSize = size * tileSize;
    if (worldPos.x > worldSize.x || worldPos.y > worldSize.y)
        return { -1, -1 };
    return {
      static_cast<int>(worldPos.x / tileSize.x),
      static_cast<int>(worldPos.y / tileSize.y)
    };
}
