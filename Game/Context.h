//
// Created by matt on 9/17/18.
//

#ifndef ROGUELIKE_CONTEXT_H
#define ROGUELIKE_CONTEXT_H

namespace pure
{
    struct Renderer;
    struct Window;
}

struct Context
{
    pure::Renderer* renderer;
    pure::Window* window;
};

#endif //ROGUELIKE_CONTEXT_H
