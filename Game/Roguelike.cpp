//
// Created by matt on 9/16/18.
//

#include <string>
#include <fstream>
#include <sstream>
#include <Pure2D/Graphics.h>
#include <Pure2D/PureMath.h>
#include <Pure2D/Window/WindowEvent.h>
#include <Pure2D/System/Defer.h>
#include <Pure2D/Audio/Audio.h>
#include <Pure2D/System/Logging.h>
#include "Game/Spritemap.h"
#include "Game/Roguelike.h"
#include "Game/Context.h"
#include "Entities/Knight.h"
#include "Game/AssetManager.h"
#include <iostream>

constexpr float TILE_SIZE = 25.f;
using namespace pure;

static std::string fileToStr(const char* filename);
static void spawnPlayer(Entity& player, LevelMap& level);


Roguelike::Roguelike()
{
	initAudio();
}

Roguelike::~Roguelike()
{
	terminateAudio();
}

pure::Window & Roguelike::window() { return m_window; }

pure::Camera & Roguelike::camera() { return m_renderer.cam; }

Entity *Roguelike::getPlayerEntity()
{
    return entities.get(player.entityId);
}

bool Roguelike::isInView(const pure::Rect<float>& r)
{
	const Rectf view = { m_renderer.cam.position.x, m_renderer.cam.position.y, float(m_window.width()), float(m_window.height()) };
	
	return (r.x >= view.x)
		&& (r.y >= view.y)
		&& (r.right() <= view.right())
		&& (r.bottom() <= view.bottom());
}

void Roguelike::update(float dt)
{
	player.handleLiveInput(*this, dt);
	const Vec3f midViewOffset = -Vec3f(static_cast<Vec2f>(m_window.size() / 2));
	m_renderer.cam.position = getPlayerEntity()->transform.position() + midViewOffset;

}

void Roguelike::load()
{
	{
		std::string fragEffect = fileToStr("Shaders/circle.frag.glsl");
		m_spotShader = Shader::fromTemplate(nullptr, fragEffect.c_str());
		m_spotShader.locations.push_back(m_spotShader.getLocation("mousePos"));
	}

	m_font.loadFromFile("C:\\Windows\\Fonts\\Arial.ttf");
	m_font.loadSize(46);
	m_text = m_font.makeText("Sample Text.", 46);
	m_text.setPosition(Vec2f(150.f, 150.f));
	m_text.color = { 1.f, 0.f, 0.f, 1.f };
	m_sound = AudioSource::make();
	SoundBuffer buff = SoundBuffer::make();
	buff.loadFromFile("Assets/sword.wav");
	m_sound.bindBuffer(buff);

	m_text.setString("A Longer Sample \nText Example Thingymajig\n");

	//m_text.setChar('B', 2);
	//m_text.setSubString("ayyyla", 6, 9);
	//m_text.resize(100);
	//m_text.setSubString("lmaoooooo", 9, 40);

	AssetManager& manager = AssetManager::get();
	manager.loadTexture("Assets/sewer.png");
	manager.loadTexture("Assets/knight.png");
	level.emplace(*AssetManager::get().get<Texture>("Assets/sewer.png"), Vec2i(50, 50),Vec2i(int(TILE_SIZE), int(TILE_SIZE)));

	WindowEvent event = {};

	m_window.setTitle("Roguelike");

	m_renderTexture = RenderTexture::make(m_window.width(), m_window.height());


#if 0
	std::string effectSrc = fileToStr("Shaders/light.frag.glsl");

	std::string lightFragShader;
	lightFragShader.resize(Shader::getFragShaderSize(effectSrc.size()));
	Shader::createFragShader(&lightFragShader[0], effectSrc.c_str());

	std::string lightVertShader;
	lightVertShader.resize(Shader::getDefaultVertShaderSize(false));
	Shader::createDefaultVertShader(&lightVertShader[0], false);


	Shader litShader = Shader::createSrc(lightVertShader.c_str(), lightFragShader.c_str());

	level->map.setFragShader(lightFragShader);

	{
		constexpr float constant = 1.f;
		constexpr float linear = 0.007f;
		constexpr float quadratic = 0.0002f;
		constexpr Vec3f ambient = Vec3f::single(0.5f);
		constexpr Vec3f diffuse = Vec3f::single(.8f);

		// TODO: Use a Uniform Buffer instead.
		litShader.setUniform("u_light.constant", constant);
		litShader.setUniform("u_light.linear", linear);
		litShader.setUniform("u_light.quadratic", quadratic);
		litShader.setUniform("u_light.ambient", ambient);
		litShader.setUniform("u_light.diffuse", diffuse);

		const Shader& mapShader = level->map.shader();
		mapShader.setUniform("u_light.constant", constant);
		mapShader.setUniform("u_light.linear", linear);
		mapShader.setUniform("u_light.quadratic", quadratic);
		mapShader.setUniform("u_light.ambient", ambient);
		mapShader.setUniform("u_light.diffuse", diffuse);
	}
#endif

	{
		std::unique_ptr<Knight> knight = std::make_unique<Knight>(Vec2f(TILE_SIZE, TILE_SIZE));
		knight->transform.move({ 0.f, 0.f, 1.f });
		knight->transform.setSize({ TILE_SIZE, TILE_SIZE });

		knight->m_mesh = Mesh::quad({ 0, 4 * 32, 32, 32 }, *manager.get<Texture>("Assets/knight.png"));
		knight->velocity = { 100.f, 100.f };
		// knight->mesh.shader = litShader;

		player.entityId = entities.add(std::move(knight));
	}

	level->map.addTileRect({ 16, 16 * 3, 16, 16 }, Spritemap::Tile::WALL);
	level->map.addTileRect({ 0, 0, 16, 16 }, Spritemap::Tile::FLOOR);

	level->numRooms = { 3, 5 };
	level->roomSize = { 4, 15 };
	level->generate();

	Quad q = Quad::make();

	Vertex2D squareVerts[5] = {
		{ 0.f, 0.f, 0.f,   0.f, 0.f,   1.f, 1.f, 1.f, 1.f },
		{ 1.f, 0.f, 0.f,   0.f, 0.f,   1.f, 1.f, 1.f, 1.f },
		{ 1.f, 1.f, 0.f,   0.f, 0.f,   1.f, 1.f, 1.f, 1.f },
		{ 0.f, 1.f, 0.f,   0.f, 0.f,   1.f, 1.f, 1.f, 1.f },
		{ 0.f, 0.f, 0.f,   0.f, 0.f,   1.f, 1.f, 1.f, 1.f },
	};

	m_cursorOutline = Mesh::make(squareVerts, 5, DrawPrimitive::LINE_STRIP);

	m_outlineTransf = Transform::make();

	{
		spawnPlayer(*getPlayerEntity(), *level);
		const Vec3f midViewOffset = -Vec3f(static_cast<Vec2f>(m_window.size() / 2));
		m_renderer.cam.position = getPlayerEntity()->transform.position() + midViewOffset;
	}
}

void Roguelike::draw()
{
	auto* player = static_cast<Knight*>(getPlayerEntity());

#if 0
	{
		Vec3f lightPos = player->transform.position() + Vec3f(player->transform.size().x / 2.f, player->transform.size().y / 2.f, 0.f);
		lightPos.z = 5.f;
		litShader.setUniform("u_light.position", lightPos);
		level->map.shader().setUniform("u_light.position", lightPos);
	}
#endif

	m_renderer.beginDrawTexture(m_renderTexture);
	{
		m_renderer.clear();
		m_renderer.draw(level->map);
		m_renderer.drawMesh(m_cursorOutline, m_outlineTransf.modelMatrix());
		m_renderer.draw(*player);
		m_renderer.draw(m_text);
	}
	m_renderer.endDrawTexture();

	Quad q = Quad::make();

	const Vec3f camPos = m_renderer.cam.position;
	m_renderer.cam.position = {};

	m_spotShader.setUniformIndx(2, m_window.mousePos());

	Transform t = Transform::make();
	t.setPosition({ 0.f, 0.f, 5.f });
	t.setSize({ float(m_window.width()), float(m_window.height()) });
	m_renderer.drawQuad(q, &t.modelMatrix(), m_spotShader, &m_renderTexture.texture);

	m_renderer.cam.position = camPos;
	//m_renderer.draw(level->map);
	//m_renderer.drawMesh(m_cursorOutline, m_outlineTransf.modelMatrix());
	//m_renderer.draw(*player);
	//m_renderer.draw(m_text);
}

void Roguelike::windowEvent(WindowEvent& event)
{
#if 1
	if (event.type == WindowEvent::Type::MouseMove)
	{
		const Vec2f mousePos = event.mousePos.toVec2();

		const Vec3f worldPos = screenToWorldPos(mousePos);
		const Vec2i pos = static_cast<Vec2i>(Vec2f(worldPos.x, worldPos.y) / static_cast<Vec2f>(level->map.tileSize));

		m_outlineTransf.setPosition({ static_cast<Vec2f>(pos) * static_cast<Vec2f>(level->map.tileSize), 10.f });
		m_outlineTransf.setSize(static_cast<Vec2f>(level->map.tileSize));
	}
#endif
	if (event.type == WindowEvent::Type::KeyRelease)
	{

		switch (event.key)
		{
		case Key::ESCAPE:
			m_window.isActive = false;
			break;
		case Key::F11:
			m_window.toggleFullscreenWindowed();
			break;
		case Key::UP:
			m_renderer.cam.position.y += -TILE_SIZE;
			break;
		case Key::LEFT:
			m_renderer.cam.position.x += -TILE_SIZE;
			break;
		case Key::RIGHT:
			m_renderer.cam.position.x += TILE_SIZE;
			break;
		case Key::DOWN:
			m_renderer.cam.position.y += TILE_SIZE;
			break;
		case Key::G:
			level->map.resetTiles();
			level->generate();
			spawnPlayer(*getPlayerEntity(), *level);
		case Key::F:
			m_sound.play();
		}


		if (event.key == Key::ESCAPE)
			m_window.isActive = false;

		if (event.key == Key::F11)
			m_window.toggleFullscreenWindowed();

	}

	player.handleInput(*this, event);


	//if (!isInView(getPlayerEntity()->boundingRect()))
	//{
	//	const Vec3f midViewOffset = -Vec3f(static_cast<Vec2f>(m_window.size() / 2));
	//	m_renderer.cam.position = getPlayerEntity()->transform.position() + midViewOffset;
	//}

	if (event.type == WindowEvent::Type::MouseScroll)
	{
		m_renderer.zoom(-event.scrollOffset.y * 50.f);
	}
}


void spawnPlayer(Entity& player, LevelMap& level)
{
	const int randRoomIndx = random(0, int(level.rooms.size() - 1));
	Recti& room = level.rooms[randRoomIndx];
	Vec2i roomPos = { random(room.x, room.right() - 1), random(room.y, room.bottom() - 1) };

	player.transform.setPosition(static_cast<Vec2f>(roomPos * level.map.tileSize));
}


std::string fileToStr(const char* filename)
{
	std::ifstream file;
	std::stringstream ss;

	file.open(filename);
	ss << file.rdbuf();
	return ss.str();
}