//
// Created by matt on 9/16/18.
//

#ifndef ROGUELIKE_PLAYER_H
#define ROGUELIKE_PLAYER_H

#include <Pure2D/Window/WindowEvent.h>
#include <Pure2D/System/Time.h>

struct EntityManager;
struct Roguelike;

struct Player
{

    void handleInput(Roguelike& g, pure::WindowEvent event);
	void handleLiveInput(Roguelike& g, float dt);

    uint32_t entityId;
};


#endif //ROGUELIKE_PLAYER_H
