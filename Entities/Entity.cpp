//
// Created by matt on 9/16/18.
//

#include "Entity.h"
#include <Pure2D/Math/Rect.h>

using namespace pure;

Entity::~Entity() {}

pure::Rect<float> Entity::boundingRect()
{
	const Vec3f pos = transform.position();
	const Vec2f size = transform.size();
	return {
		pos.x, pos.y,
		size.x, size.y
	};
}
