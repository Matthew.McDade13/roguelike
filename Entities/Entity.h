//
// Created by matt on 9/16/18.
//

#ifndef ROGUELIKE_ENTITY_H
#define ROGUELIKE_ENTITY_H

#include <cinttypes>
#include <Pure2D/Graphics/Transform.h>

namespace pure
{
	template<typename T> struct Rect;
}

struct Entity
{
    virtual ~Entity() = 0;

    uint32_t id;
    pure::Transform transform;

	pure::Rect<float> boundingRect();
};


#endif //ROGUELIKE_ENTITY_H
