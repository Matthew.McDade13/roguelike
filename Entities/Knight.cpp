//
// Created by matt on 9/16/18.
//

#include "Knight.h"
#include "Game/AssetManager.h"
#include <Pure2D/Graphics/Texture.h>
#include <Pure2D/Graphics/Renderer.h>

using namespace pure;

Knight::Knight(pure::Vec2f size)
{
	transform = Transform::make();
	transform.move({ 0.f, 0.f, -.5f });
	transform.setSize(size);

	m_mesh = Mesh::quad({ 0, 4 * 32, 32, 32 }, *AssetManager::get().get<Texture>("Assets/knight.png"));
	velocity = { 100.f, 100.f };

	collisionPoints.push_back(Vec2f(0.f, size.y));
	collisionPoints.push_back(Vec2f(size.x * .5f, size.y));
	collisionPoints.push_back(Vec2f(size.x, size.y));

	// knight->mesh.shader = litShader;
}

void Knight::draw(pure::Renderer &renderer)
{
    renderer.drawMesh(m_mesh, transform.modelMatrix());
}
