//
// Created by matt on 9/16/18.
//

#ifndef ROGUELIKE_KNIGHT_H
#define ROGUELIKE_KNIGHT_H

#include "Entity.h"
#include <vector>
#include <Pure2D/Math/Vec2.h>
#include <Pure2D/Graphics/Mesh.h>
#include <Pure2D/Graphics/Renderable.h>

namespace pure { struct Renderer; }

struct Knight : public Entity, public pure::Renderable
{
	Knight(pure::Vec2f size);

	pure::Vec2f velocity = {};
    pure::Mesh m_mesh;
	// relative to top left corner
	std::vector<pure::Vec2f> collisionPoints;

    void draw(pure::Renderer& renderer) final;
};


#endif //ROGUELIKE_KNIGHT_H
