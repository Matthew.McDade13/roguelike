//
// Created by matt on 9/16/18.
//

#include "Player.h"
#include "Knight.h"

#include <algorithm>

#include "Game/EntityManager.h"
#include "Game/LevelMap.h"
#include <Pure2D/Window/WindowEvent.h>
#include "Game/Roguelike.h"

using namespace pure;

static inline void handlePlayerMove(Vec3f moveTo, Knight& player, const Spritemap& map);
static inline Vec2i toTileCoords(Vec3f pos, const Spritemap& map);

void Player::handleInput(Roguelike& g, pure::WindowEvent event)
{

  //  if (event.type == WindowEvent::Type::KeyRelease)
  //  {
  //      auto* player = g.entities.get(entityId);
		//const Vec3f playerPos = player->transform.position();
		//const Vec2f tileSize = static_cast<Vec2f>(g.level->map.tileSize);

		//Spritemap& map = g.level->map;

  //      switch (event.key)
  //      {
		//	case Key::W: 
		//		handlePlayerMove(playerPos + Vec3f(0.f, -tileSize.y), *player, map);			
  //          break;
  //          case Key::A:
		//		handlePlayerMove(playerPos + Vec3f(-tileSize.x, 0.f), *player, map);
  //          break;
  //          case Key::S:
		//		handlePlayerMove(playerPos + Vec3f(0.f, tileSize.y), *player, map);
  //          break;
  //          case Key::D:
		//		handlePlayerMove(playerPos + Vec3f(map.tileSize.x, 0.f), *player, map);
  //          break;
  //      }
  //  }
}

void Player::handleLiveInput(Roguelike & g, float dt)
{
	auto* player = static_cast<Knight*>(g.entities.get(entityId));
	const Vec3f vel = Vec3f(player->velocity);

	const Spritemap& map = g.level->map;
	
	// TODO: Collision detection still not quite right just yet.
	if (isKeyPressed(Key::W))
	{
		const Vec3f moveTo = player->transform.position() + Vec3f(0.f, -vel.y) * dt;
		handlePlayerMove(moveTo, *player, map);
	}
	if (isKeyPressed(Key::A))
	{
		const Vec3f moveTo = player->transform.position() + Vec3f(-vel.x, 0.f) * dt;
		handlePlayerMove(moveTo, *player, map);
	}
	if (isKeyPressed(Key::S))
	{
		const Vec3f moveTo = player->transform.position() + Vec3f(0.f, vel.y) * dt;
		handlePlayerMove(moveTo, *player, map);
	}
	if (isKeyPressed(Key::D))
	{
		const Vec3f moveTo = player->transform.position() + Vec3f(vel.x, 0.f) * dt;
		handlePlayerMove(moveTo, *player, map);
	}
	
}

void handlePlayerMove(Vec3f moveTo, Knight& player, const Spritemap& map)
{

	for (const auto& p : player.collisionPoints)
	{
		const Vec2i tileCoords = static_cast<Vec2i>((Vec2f(moveTo) + p) / static_cast<Vec2f>(map.tileSize));
		if (map.tileTypeAt(tileCoords) == Spritemap::Tile::WALL)
			return;
	}

	player.transform.setPosition(moveTo);
}

Vec2i toTileCoords(Vec3f pos, const Spritemap& map)
{
	return static_cast<Vec2i>(Vec2f(pos) / static_cast<Vec2f>(map.tileSize));
}