//
// Created by matt on 9/16/18.
//

#ifndef ROGUELIKE_SPARSEARRAY_H
#define ROGUELIKE_SPARSEARRAY_H

#include <vector>
#include <cassert>
#include <type_traits>
#include <cinttypes>

template<typename T>
struct IntrusiveList
{
private:
    struct Item
    {
        T item;
        size_t index;
        Item* next;
    };

public:
    using type = T;

    IntrusiveList(const IntrusiveList&) = delete;
    IntrusiveList& operator=(const IntrusiveList&) = delete;

    IntrusiveList() = default;

    explicit IntrusiveList(uint32_t size)
    {
        m_items.resize(size);

        assignDefaultNextPtrs();
    }

    IntrusiveList(IntrusiveList<T> &&other) noexcept:
    m_items(std::move(other.m_items))
    {
        m_firstAvailable = other.m_firstAvailable;
        other.m_firstAvailable = nullptr;
    }

    size_t size() { return m_items.size(); }

    T& make()
    {
        assert(m_firstAvailable);

        Item* item = m_firstAvailable;

        m_firstAvailable = item->next;

        //*item = { };
        item->next = m_firstAvailable;

        return static_cast<T&>(*item);
    }

    // TODO: Find a way to clear previous data in a safe manner
    void destroy(T& item)
    {
        Item& destroyTarget = static_cast<Item&>(item);

        destroyTarget = { };

        destroyTarget.next = m_firstAvailable;

        m_firstAvailable = &destroyTarget;
    }

    void destroy(int targetIndx)
    {
        assert(targetIndx < m_items.size() && targetIndx >= 0);

        Item* target = &m_items[targetIndx];

        target->next = m_firstAvailable;
        m_firstAvailable = target;
    }

    /**
    * Resize pool to given size.
    * Destroys and resets all objects in pool before resizing.
    * Does nothing if given newSize is same as current size.
    *
    * @param size new pool size
    */
    void resize(uint32_t newSize)
    {
        if (newSize == m_items.size()) return;
        if (m_items.empty())
        {
            m_items.resize(newSize);
            assignDefaultNextPtrs();
            return;
        }

        reset();
        m_items.resize(newSize);
        assignDefaultNextPtrs();
    }

    // Destroy and set all active objects to not active state
    void reset()
    {
        auto objSize = static_cast<int>(m_items.size());
        for (int i = objSize - 1; i >= 0; i--)
            destroy(i);
    }


private:

    std::vector<Item> m_items;
    Item* m_firstAvailable = nullptr;

    void assignDefaultNextPtrs()
    {
        m_firstAvailable = &m_items[0];

        for (size_t i = 0; i < m_items.size() - 1; i++)
        {
            m_items[i].next = &m_items[i + 1];
        }

        m_items.back().next = nullptr;

    }

};


#endif //ROGUELIKE_SPARSEARRAY_H
