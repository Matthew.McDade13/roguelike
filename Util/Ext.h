//
// Created by matt on 9/29/18.
//

#ifndef ROGUELIKE_EXT_H
#define ROGUELIKE_EXT_H

#include <unordered_map>
#include <string>
#include <type_traits>
#include <Pure2D/Math/Vec2.h>
#include <Pure2D/Math/Vec3.h>
#include <Pure2D/Math/Vec4.h>


template <typename K, typename V>
V* mapGet(std::unordered_map<K, V>& map, const K& key)
{
    auto itr = map.find(key);
    if (itr == map.end()) return nullptr;

    return &itr->second;
}

template<typename T>
std::string toString(pure::Vec2<T> vec)
{
    using namespace std::string_literals;

    std::string result{};
    result += "[Vector2]: "s + "("s + std::to_string(vec.x) + ", "s + std::to_string(vec.y) + ")"s;
    return result;
}

template<typename T>
std::string toString(const pure::Vec3<T>& vec)
{
    using namespace std::string_literals;

    std::string result{};
    result += "[Vector3]: "s + "("s + std::to_string(vec.x) + ", "s + std::to_string(vec.y) + + ", " + std::to_string(vec.z) +")"s;
    return result;
}


template<typename T>
std::string toString(const pure::Vec4<T>& vec)
{
    using namespace std::string_literals;

    std::string result{};
    result += "[Vector4]: "s + "("s + std::to_string(vec.x) + ", "s + std::to_string(vec.y) + + ", " + std::to_string(vec.z) +", " + std::to_string(vec.w) + ")"s;
    return result;
}



#endif //ROGUELIKE_EXT_H
