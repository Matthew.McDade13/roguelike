//
// Created by matt on 9/15/18.
//

#ifndef ROGUELIKE_UTIL_H
#define ROGUELIKE_UTIL_H

// TODO: Move this Pure2D Lib

#include <algorithm>

template<typename T>
constexpr T& indexFlat2DArr(T* arr, int width, int x, int y)
{
    return arr[y * width + x];
}

template <typename T>
constexpr int signum(T val)
{
    return (T(0) < val) - (val < T(0));
}

template <typename T>
constexpr T clamp(T val, T minVal, T maxVal)
{
    return std::min(std::max(val, minVal), maxVal);
}


#endif //ROGUELIKE_UTIL_H
