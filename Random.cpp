//
// Created by matt on 9/13/18.
//

#include "Random.h"
#include <random>
#include <cassert>


float randomf(float max)
{
    return randomf(0.f, max);
}

float randomf(float min, float max)
{
    assert(min <= max);
    static std::mt19937 g((std::random_device())());
    return std::uniform_real_distribution<float>(min, max)(g);
}

int random(int max)
{
    return random(0, max);
}

int random(int min, int max)
{
    assert(min <= max);
    static std::mt19937 g((std::random_device())());
    return std::uniform_int_distribution<>(min, max)(g);
}

float randomf(Range<float> range)
{
    return randomf(range.min, range.max);
}

int random(Range<int> range)
{
    return random(range.min, range.max);
}
