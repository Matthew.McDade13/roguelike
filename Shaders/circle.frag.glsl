
uniform vec2 mousePos;

const float radius = 100.0;
const float radSquared = radius * radius;

vec4 effect(vec4 color, sampler2D tex, vec2 texCoords, vec3 fragPos)
{
    float dx = fragPos.x - mousePos.x;
    float dy = fragPos.y - mousePos.y;
    float dist = (dx * dx) + (dy * dy);

    if (dist <= radSquared)
        return texture(tex, texCoords) * vec4(1.0, 0.0, 0.0, 0.5);

    return texture(tex, texCoords);
}

