

vec4 effect(vec4 color, sampler2D tex, vec2 texCoords, vec3 fragPos)
{
    vec4 col = texture(tex, texCoords);
    if (col ==  vec4(1.0))
    {
        discard;
    }

    return texture(tex, texCoords);
}

