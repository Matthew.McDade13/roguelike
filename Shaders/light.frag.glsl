

struct Light
{
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    float constant;
    float linear;
    float quadratic;
};


uniform vec3 u_lightPos;
uniform Light u_light;

const vec3 normal = vec3(0.0, 0.0, 1.0);
const vec3 lightColor = vec3(1.0);

vec2 texCoord = vec2(0);

float calcAttenuation(vec3 fragPos);
vec3 calcAmbient(sampler2D tex);
vec3 calcDiffuse(vec3 lightDirection, sampler2D tex);

float calcAttenuation(vec3 fragPos)
{
    float dist = length(u_light.position - fragPos);
    return 1.0 / (u_light.constant + u_light.linear * dist + u_light.quadratic * (dist * dist));  
}

vec3 calcDiffuse(vec3 lightDirection, sampler2D tex)
{    
    float diffuseImpact = max(dot(normal, lightDirection), 0.0);
    return diffuseImpact * vec3(texture(tex, texCoord)) * u_light.diffuse;
}

vec3 calcAmbient(sampler2D tex)
{
    return vec3(texture(tex, texCoord)) * u_light.ambient;
}

vec4 effect(vec4 color, sampler2D tex, vec2 texCoords, vec3 fragPos)
{
    texCoord = texCoords;
    vec3 lightDir = normalize(u_light.position - fragPos);
    vec3 ambient = calcAmbient(tex);
    vec3 diffuse = calcDiffuse(lightDir, tex);
    float attenuation = calcAttenuation(fragPos);

    ambient *= attenuation;
    diffuse *= attenuation;

    vec3 result = (ambient + diffuse);
	return vec4(result, 1.0);
}

