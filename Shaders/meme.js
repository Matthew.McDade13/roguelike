

function addVec(veca, vecb)
{
    return {
        x: veca.x + vecb.x,
        y: veca.y + vecb.y
    };
}

function subVec(veca, vecb)
{
    return {
        x: veca.x - vecb.x,
        y: veca.y - vecb.y
    };
}

function scaleVec(vec, scalar)
{
    return {
        x: vec.x * scalar,
        y: vec.y * scalar
    };
}


const u = { x: 2, y: 7 };
const v = { x: 7, y: 2 };
const su = scaleVec(u, 4);
const sv = scaleVec(v, 3);


// console.log(addVec(u, v));
// console.log(subVec(u, v));
// console.log(addVec(su, sv));

let result = Math.atan(-5/Math.sqrt(11)) * (180/Math.PI);

if (result < 0) result += 360;

console.log(result);