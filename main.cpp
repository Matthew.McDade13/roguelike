#include "Game/Roguelike.h"

int main()
{
	Roguelike g{};
	g.run();
    return 0;
}